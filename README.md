# Workshop Data and research management

This repository includes all material to reproduce the first results and Figure in Gneezy et al. (2007), and the test that was replicated as part of the Social Sciences Replication Project.

## Folder Structure

- analysis/
    - [Files related to data analysis]
  - orig_analysis/
    - [Original analysis files or results]
  - report_orig_results.rmd
    - [R Markdown file generating a report based on original results]
  - rep_analysis/
    - [Files related to replicated analysis or repeated analysis]
- data/
    - [Data files]
    - orig_code_book.json
        - [A JSON file containing the original codebook for the dataset.]
    - orig_code_book.md
        - [A Markdown file providing initial documentation for the original dataset.]
    - orig_data.txt
        - [The original dataset in text format.]
- .gitignore
    - [File specifying untracked files to ignore]
- LICENSE.txt
    - [Creative Commons 4.0 Attribution License]
- README.md
    - [README file of the repository]

## File Naming Convention
Files are named descriptively, reflecting their content or purpose, followed by the '.rmd' extension for R Markdown files.
For other files like data files or the README, standard conventions appear to be followed without any specific naming pattern mentioned in the provided information.

Hallo dies ist ein Test