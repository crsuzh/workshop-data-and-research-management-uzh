---
title: 'Report to reproduce main results'
subtitle: 'Avoiding overhead aversion in charity'
author: Workshop Data and research management
date: last-modified
format:
  html:
    toc: true
    df-print: kable
    embed-resources: true
    code-fold: true
    code-block-bg: '#E6EEF0'
    code-block-border-left: '#9BBDC5'
---

# Data import

No need to even download the data, it can readily be loaded into R using a URL as the original data is openly available (with a license) [online](https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/27366).

```{r}
URL <- "https://dataverse.harvard.edu/api/access/datafile/:persistentId?persistentId=doi:10.7910/DVN/27366/VOLGZD"
data <- read.table(URL, header = TRUE, fill = TRUE)
```

# Data overview

The following variables are present in the data:

```{r}
tab <- as.data.frame(colnames(data))
colnames(tab) <- "Variable names"
tab
```


# Data Processing

First, we aggregate the data so that we have the counts of allocation for each treatments.

```{r}
# table ignores missing values, but we have no missing so then it is okey
tab_control   <- table(data$allocation[data$noover == 1])
tab_low       <- table(data$allocation[data$low == 1])
tab_high      <- table(data$allocation[data$high == 1])
tab_lowcover  <- table(data$allocation[data$lowcover == 1])
tab_highcover <- table(data$allocation[data$highcover == 1])
```

# First reported result

We reproduce (successfully) the first reported statistical result. It is a test whether two proportions (treatment controls vs. treatment low) were statistically different.

```{r}
stats = prop.test(x = c(tab_control[2],tab_low[2]), 
                  n = c(sum(tab_control), sum(tab_low)), correct = FALSE
)
```

::: {.callout-note}
A test of equal proportions was not statistically significant (z = `r round(sqrt(stats$statistic), digits=2)`; p = `r round(stats$p.value, digits=2)`).
::: 

# Reproducing Figure 1

To reproduce the figure we need the proportion of participants choosing charity:water, for each treatment combination of cover and overhead. Additionally, we need some kind of error bars. The authors chose to use $\pm$ one standard error. We will generate the same figure, but also provide a additional, improved figure by using _Wilson_ confidence intervals (CI), through the R function `prop.test()`. 

## Calculating CIs

```{r}
prop_control   <- prop.test(x = tab_control["1"], sum(tab_control))
prop_low       <- prop.test(x = tab_low["1"], sum(tab_low))
prop_high      <- prop.test(x = tab_high["1"], sum(tab_high))
prop_lowcover  <- prop.test(x = tab_lowcover["1"], sum(tab_lowcover))
prop_highcover <- prop.test(x = tab_highcover["1"], sum(tab_highcover))
```

## Calculating standard errors of proportions

```{r}
# Helper function to compute SE of a proportion, from a table of a binary var
se_prop <- function(tab){
  n <- sum(tab)
  p <- tab[1]/n
  return(sqrt(p * (1 - p) / n))
}

se_low <- se_prop(tab_low)
se_high <- se_prop(tab_high) 
se_lowcover <- se_prop(tab_lowcover) 
se_highcover <- se_prop(tab_highcover) 
se_control <- se_prop(tab_control) 
```

## Figure 1 (SE as errorbars)

```{r, echo = FALSE, fig.keep="last"}
plot(x = c(5, 50), y = c(prop_low$estimate, prop_high$estimate), 
     ylim = c(0, 1), type = "o", col = "darkblue", xlim = c(-.5, 50.5),
     xlab = "Overhead level", ylab = "Proportion that chose charity:water")
segments(x0 = c(5, 50), x1 = c(5, 50),
         y0 = c(prop_low$estimate - se_low, prop_high$estimate - se_high),
         y1 = c(prop_low$estimate + se_low, prop_high$estimate + se_high), col = "darkblue")
lines(x = c(5, 50), y = c(prop_lowcover$estimate, prop_highcover$estimate), 
      type = "o", col = "darkgreen")
segments(x0 = c(5, 50), x1 = c(5, 50),
         y0 = c(prop_lowcover$estimate - se_lowcover, prop_highcover$estimate - se_highcover),
         y1 = c(prop_lowcover$estimate + se_lowcover, prop_highcover$estimate + se_highcover),
         col = "darkgreen")
points(x = 0, y = prop_control$estimate, col = "magenta")

segments(x0 = 0, x1 = 0, y0 = prop_control$estimate - se_control,
         y1 = prop_control$estimate + se_control,col = "magenta")
```


## Figure 1 (95%-CIs as errorbar)

```{r, echo = FALSE, fig.keep="last"}
plot(x = c(5, 50), y = c(prop_low$estimate, prop_high$estimate), 
     ylim = c(0, 1), type = "o", col = "darkblue", xlim = c(-.5, 50.5),
     xlab = "Overhead level", ylab = "Proportion that chose charity:water")
segments(x0 = c(5, 50), x1 = c(5, 50),
         y0 = c(prop_low$conf.int[1], prop_high$conf.int[1]),
         y1 = c(prop_low$conf.int[2], prop_high$conf.int[2]), col = "darkblue")
lines(x = c(5, 50), y = c(prop_lowcover$estimate, prop_highcover$estimate), 
      type = "o", col = "darkgreen")
segments(x0 = c(5, 50), x1 = c(5, 50),
         y0 = c(prop_lowcover$conf.int[1], prop_highcover$conf.int[1]),
         y1 = c(prop_lowcover$conf.int[2], prop_highcover$conf.int[2]),
         col = "darkgreen")
points(x = 0, y = prop_control$estimate, col = "magenta")

segments(x0 = 0, x1 = 0, y0 = prop_control$conf.int[1],
         y1 = prop_control$conf.int[2],col = "magenta")
```


# Test to be replicated in Social Science Replicability Project

```{r replication}
# Two-sample z-test of proportion
z.prop <- function(x1, x2, n1, n2) {
  numerator    <- (x1/n1) - (x2/n2)
  p.common     <- (x1+x2) / (n1+n2)
  denominator  <- sqrt(p.common * (1-p.common) * (1/n1 + 1/n2))
  z.prop.stat  <- numerator / denominator
  return(z.prop.stat)
}

z <- z.prop(x1 = tab_highcover["1"], x2 = tab_high["1"], 
            n1 = sum(tab_highcover),
            n2 = sum(tab_high))
```


::: {.callout-note}
**A test of equal proportions was statistically significant (z = `r round(z, digits=2)`; p = `r formatC(2*pnorm(-abs(z)))`).**
::: 



